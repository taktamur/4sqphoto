//
//  UIKit+UtilTest.m
//  4sqPhoto
//
//  Created by tak on 2014/01/29.
//  Copyright (c) 2014年 taktamur. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "UIKit+Util.h"

@interface UIKit_UtilTest : XCTestCase

@end

@implementation UIKit_UtilTest

- (void)setUp
{
    [super setUp];
    // Put setup code here; it will be run once, before the first test case.
}

- (void)tearDown
{
    // Put teardown code here; it will be run once, after the last test case.
    [super tearDown];
}

- (void)testExample
{
    UISearchBar *searchBar = [[UISearchBar alloc]init];
    NSArray *textFields = [searchBar viewsFromSubviewsByMatchProtocol:@protocol(UITextInputTraits)];
    XCTAssertNotNil(textFields, @"not nil.");
    XCTAssertEqual(textFields.count, (NSUInteger)1, @"count");
    XCTAssertTrue([textFields[0] conformsToProtocol:@protocol(UITextInputTraits)],@"protocol");
    id<UITextInputTraits> textInputTraits = (id<UITextInputTraits>)textFields[0];
    XCTAssertNoThrow(searchBar.enablesReturnKeyAutomatically=YES);
    XCTAssertTrue(textInputTraits.enablesReturnKeyAutomatically);
    XCTAssertNoThrow(searchBar.enablesReturnKeyAutomatically=NO);
    XCTAssertFalse(textInputTraits.enablesReturnKeyAutomatically);
    
}

@end
