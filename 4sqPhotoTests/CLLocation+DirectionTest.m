//
//  CLLocation+DirectionTest.m
//  4sqPhoto
//
//  Created by Takafumi Tamura on 2014/01/28.
//  Copyright (c) 2014年 taktamur. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "CLLocation+Direction.h"

@interface CLLocation_DirectionTest : XCTestCase

@end

@implementation CLLocation_DirectionTest

- (void)setUp
{
    [super setUp];
    // Put setup code here; it will be run once, before the first test case.
}

- (void)tearDown
{
    // Put teardown code here; it will be run once, after the last test case.
    [super tearDown];
}

- (void)testExample
{
    CLLocation *center = [[CLLocation alloc]initWithLatitude:35 longitude:135];
    CLLocation *topRight = [[CLLocation alloc]initWithLatitude:36 longitude:136];
    CLLocation *middleRight = [[CLLocation alloc]initWithLatitude:35 longitude:136];
    CLLocation *bottomRight = [[CLLocation alloc]initWithLatitude:34 longitude:136];
    CLLocation *bottomCenter = [[CLLocation alloc]initWithLatitude:34 longitude:135];
    CLLocation *bottomLeft = [[CLLocation alloc]initWithLatitude:34 longitude:134];
    CLLocation *middleLeft = [[CLLocation alloc]initWithLatitude:35 longitude:134];
    CLLocation *topLeft = [[CLLocation alloc]initWithLatitude:36 longitude:134];
    CLLocation *topCenter = [[CLLocation alloc]initWithLatitude:36 longitude:135];
    
    XCTAssertEqualWithAccuracy([center aboutAzimuthToLocation:topCenter], 0, 0.1, @"topCenter");
    XCTAssertEqualWithAccuracy([center aboutAzimuthToLocation:topRight], 45, 0.1, @"topCenter");
    XCTAssertEqualWithAccuracy([center aboutAzimuthToLocation:middleRight], 90, 0.1, @"topCenter");
    XCTAssertEqualWithAccuracy([center aboutAzimuthToLocation:bottomRight], 135, 0.1, @"topCenter");
    XCTAssertEqualWithAccuracy([center aboutAzimuthToLocation:bottomCenter], 180, 0.1, @"topCenter");
    XCTAssertEqualWithAccuracy([center aboutAzimuthToLocation:bottomLeft], 225, 0.1, @"topCenter");
    XCTAssertEqualWithAccuracy([center aboutAzimuthToLocation:middleLeft], 270, 0.1, @"topCenter");
    XCTAssertEqualWithAccuracy([center aboutAzimuthToLocation:topLeft], 315, 0.1, @"topCenter");
    
}

@end
