//
//  FSPVenue.h
//  4sqPhoto
//
//  Created by tak on 2014/01/18.
//  Copyright (c) 2014年 taktamur. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface FSPVenue : NSObject
@property(nonatomic)NSString *name;
@property(nonatomic)NSString *venueID;
@property(nonatomic,readonly) NSURL *photoURL;
@property(nonatomic)NSArray *photos;
@property(nonatomic)CLLocation *location;
@property(nonatomic)NSString *categoryIconPrefix;
@property(nonatomic)NSString *categoryIconSuffix;
@property(nonatomic)NSArray *tips;
@property(nonatomic,readonly) NSURL *url;
-(NSURL *)categoryIconURLWithSize:(NSUInteger)size;

@end



@interface FSPVenuePhoto : NSObject
-(id)initWithPrefix:(NSString *)prefix andSuffix:(NSString *)suffix;
@property(nonatomic,weak)FSPVenue *venue;
-(NSURL *)urlWithSize:(CGSize)size;
-(NSURL *)urlWithSize:(CGSize)size scale:(CGFloat)scale;
@end

@interface FSPVenueMapImage : NSObject
-(NSURL *)urlWithSize:(CGSize)size;
@property(nonatomic,weak)FSPVenue *venue;
@end 

@interface FSPVenueTips:NSObject
@property(nonatomic)NSString *text;
@end

@interface FSPVenue(JSONParse)
+(FSPVenue *)venueFromJSONDictionary:(NSDictionary *)jsonDictionary;
+(NSArray *)venuesArrayFromJSON:(id)json;

//+(NSArray *)photoPathsFromJSON:(id)json withSize:(NSUInteger)size;
@end

@interface FSPVenuePhoto(JSONParse)
+(NSArray *)photoPathsFromJSON:(id)json withVenue:(FSPVenue *)venue;
@end
