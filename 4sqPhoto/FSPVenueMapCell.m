//
//  FSPVenueMapCell.m
//  4sqPhoto
//
//  Created by Takafumi Tamura on 2014/01/22.
//  Copyright (c) 2014年 taktamur. All rights reserved.
//

#import "FSPVenueMapCell.h"
#import <MapKit/MapKit.h>

@interface FSPVenueMapCell()
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@end

@implementation FSPVenueMapCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)setVenueLocation:(CLLocation *)location
{
    self.mapView.centerCoordinate=location.coordinate;
    // TODO 縮尺調整
}
@end
