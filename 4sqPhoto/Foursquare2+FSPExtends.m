//
//  Foursquare2+FSPExtends.m
//  4sqPhoto
//
//  Created by tak on 2014/01/18.
//  Copyright (c) 2014年 taktamur. All rights reserved.
//

#import "Foursquare2+FSPExtends.h"

@implementation Foursquare2 (FSPExtends)
+(void)venueExploreByLocation:(CLLocation *)location
                                    callback:(Foursquare2Callback)callback
{
    
    [Foursquare2 venueExploreRecommendedNearByLatitude:@(location.coordinate.latitude)
                                             longitude:@(location.coordinate.longitude)
                                                  near:nil
                                            accuracyLL:@(location.horizontalAccuracy)
                                              altitude:@(location.altitude)
                                           accuracyAlt:nil
                                                 query:nil
                                                 limit:@(30)
                                                offset:0
                                                radius:@(500)
                                               section:@"food"
                                               novelty:nil
                                        sortByDistance:YES
                                               openNow:YES
                                                 price:nil
                                              callback:callback];
}

+(void)venueExploreByLocation:(CLLocation *)location
                   andKeyword:(NSString *)keyword
                     callback:(Foursquare2Callback)callback
{
    [Foursquare2 venueExploreRecommendedNearByLatitude:@(location.coordinate.latitude)
                                             longitude:@(location.coordinate.longitude)
                                                  near:nil
                                            accuracyLL:@(location.horizontalAccuracy)
                                              altitude:@(location.altitude)
                                           accuracyAlt:nil
                                                 query:keyword
                                                 limit:@(30)
                                                offset:0
                                                radius:@(1000)
                                               section:@"food"
                                               novelty:nil
                                        sortByDistance:YES
                                               openNow:NO
                                                 price:nil
                                              callback:callback];
}

@end
