//
//  FSPAppDelegate.h
//  4sqPhoto
//
//  Created by tak on 2014/01/17.
//  Copyright (c) 2014年 taktamur. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FSPAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
