//
//  FSPArrowCell.m
//  4sqPhoto
//
//  Created by tak on 2014/01/26.
//  Copyright (c) 2014年 taktamur. All rights reserved.
//

#import "FSPArrowCell.h"
#import <CoreLocation/CoreLocation.h>
#import "SFKImage.h"
#import "CLLocation+Direction.h"

@interface FSPArrowCell()<CLLocationManagerDelegate>
@property (weak, nonatomic) IBOutlet UIView *arrowView;
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;
@property (nonatomic)CLLocationManager *locationManager;
@property (nonatomic)UIImageView *imageView;
@end

@implementation FSPArrowCell{
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
-(void)setVenue:(FSPVenue *)venue
{
    if( self.locationManager == nil ){
        self.locationManager = [CLLocationManager new];
        self.locationManager.delegate = self;
        [self.locationManager startUpdatingLocation];
        [self.locationManager startUpdatingHeading];
    }
//    if( self.imageView == nil ){
//        
//        [SFKImage setDefaultFont:[UIFont fontWithName:@"flaticon" size:34.0f]];
//        [SFKImage setDefaultColor:[UIColor orangeColor]];
//        [SFKImage setPreferredSize:self.bounds.size];
//        self.imageView = [[UIImageView alloc]initWithFrame:self.bounds];
//        self.imageView.image = [SFKImage imageNamed:@"\ue000"];
//        [self addSubview:self.imageView];
//        
//    }
    _venue = venue;
    self.arrowView.transform = CGAffineTransformMakeRotation(0);
    self.imageView.transform =CGAffineTransformMakeRotation(0);

}

#pragma mark - CLLocationManagerDelegate
-(void)locationManager:(CLLocationManager *)manager
    didUpdateLocations:(NSArray *)locations
{
    CLLocation *location = locations[0];
    CLLocationDistance distanceMeter = [location distanceFromLocation:self.venue.location];
    self.distanceLabel.text = [NSString stringWithFormat:@"%dm",(int)distanceMeter];
    
}
-(void)locationManager:(CLLocationManager *)manager
      didUpdateHeading:(CLHeading *)newHeading
{
    NSLog( @"update heading");
    CLLocationDirection heading = newHeading.trueHeading; // 0.0〜360.0
    CLLocationDirection target = [manager.location aboutAzimuthToLocation:self.venue.location];
    CLLocationDirection displayDirection = target-heading;
    NSLog( @"venur=%@ heading=%f, target=%f, display=%f",self.venue.name,heading,target,displayDirection);
    self.arrowView.transform = CGAffineTransformMakeRotation(M_PI * displayDirection / 180.0);
    self.imageView.transform = CGAffineTransformMakeRotation(M_PI * displayDirection / 180.0);
    
}
@end
