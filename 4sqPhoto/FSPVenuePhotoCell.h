//
//  FSPPhotoViewCell.h
//  4sqPhoto
//
//  Created by tak on 2014/01/18.
//  Copyright (c) 2014年 taktamur. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FSPVenue.h"

@interface FSPVenuePhotoCell : UICollectionViewCell
@property(nonatomic)FSPVenue *venue;
@property(nonatomic)FSPVenuePhoto *venueImage;
@end
