//
//  FSPsectionView.h
//  4sqPhoto
//
//  Created by tak on 2014/01/18.
//  Copyright (c) 2014年 taktamur. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FSPVenue.h"
#import <CoreLocation/CoreLocation.h>
#import "FSPArrowView.h"

@interface FSPVenueHeaderCell : UICollectionReusableView<CLLocationManagerDelegate>
@property(nonatomic)FSPVenue *venue;
@property (weak, nonatomic) IBOutlet UIButton *nameButton;
@property (weak, nonatomic) IBOutlet UIButton *arrowButton;

@end
