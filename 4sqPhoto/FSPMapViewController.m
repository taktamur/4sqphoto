//
//  FSPMapViewController.m
//  4sqPhoto
//
//  Created by tak on 2014/01/25.
//  Copyright (c) 2014年 taktamur. All rights reserved.
//

#import "FSPMapViewController.h"
#import <MapKit/MapKit.h>

@interface FSPVenue(MapView)<MKAnnotation>
@end

@implementation FSPVenue(MapView)

-(CLLocationCoordinate2D)coordinate
{
    return self.location.coordinate;
}
-(NSString *)title
{
    return self.name;
}
-(NSString *)subtitle
{
    return nil;
}
@end

@interface FSPMapViewController ()
@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@end

@implementation FSPMapViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.mapView.userTrackingMode=MKUserTrackingModeFollow;
    self.mapView.centerCoordinate = self.venue.location.coordinate;
    [self.mapView addAnnotation:self.venue];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
