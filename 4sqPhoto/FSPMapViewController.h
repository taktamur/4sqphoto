//
//  FSPMapViewController.h
//  4sqPhoto
//
//  Created by tak on 2014/01/25.
//  Copyright (c) 2014年 taktamur. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "FSPVenue.h"

@interface FSPMapViewController : UIViewController
@property(nonatomic)FSPVenue *venue;
@end
