//
//  FSPLargePhotoViewController.h
//  4sqPhoto
//
//  Created by tak on 2014/01/20.
//  Copyright (c) 2014年 taktamur. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FSPVenue.h"

@interface FSPPhotosViewController : UICollectionViewController<UICollectionViewDataSource>
@property(nonatomic)NSArray *venueImages;
@property(nonatomic)NSUInteger venueImagesIndex;
@end
