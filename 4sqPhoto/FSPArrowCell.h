//
//  FSPArrowCell.h
//  4sqPhoto
//
//  Created by tak on 2014/01/26.
//  Copyright (c) 2014年 taktamur. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FSPVenue.h"

@interface FSPArrowCell : UICollectionViewCell
@property(nonatomic)FSPVenue *venue;
@end
