//
//  FSPPhotosViewController.h
//  4sqPhoto
//
//  Created by tak on 2014/01/18.
//  Copyright (c) 2014年 taktamur. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface FSPVenuesViewController : UICollectionViewController<UICollectionViewDataSource,CLLocationManagerDelegate,UICollectionViewDelegate,UISearchBarDelegate>

@end
