//
//  FSPPhotosViewController.m
//
//  Created by tak on 2014/01/18.
//  Copyright (c) 2014年 taktamur. All rights reserved.
//

#import "FSPVenuesViewController.h"
#import <Foursquare-API-v2/Foursquare2.h>
#import "FSPVenue.h"
#import "FSPVenuePhotoCell.h"
#import "Foursquare2+FSPExtends.h"
#import "FSPVenueHeaderCell.h"
#import "FSPPhotosViewController.h"
#import <PBWebViewController/PBWebViewController.h>
#import "FSPMapViewController.h"
#import "UIKit+Util.h"
#import "UIPinchGestureRecognizer+PAMUtil.h"
#import "UICollectionViewFlowLayout+PAMUtil.h"

@interface FSPHeaderTapGestureRecognizer: UITapGestureRecognizer
@property(nonatomic)NSUInteger section;
@end

@implementation FSPHeaderTapGestureRecognizer
@synthesize section;
@end


@interface FSPVenuesViewController ()
@property(nonatomic,readonly)NSMutableArray *venues;
@property(nonatomic)CLLocationManager *locationManager;
@property(nonatomic)CLLocation *location;
@property(weak, nonatomic) IBOutlet UISearchBar *searchBar;

@property(nonatomic)NSString *keyword;
@property(nonatomic)NSUInteger horizontalCellCount;
// ピンチイン/アウトでの拡大縮小用
@property(nonatomic)PAMPinchGestureZoomStatus zoomingStatus; // 今現在ズームしている方向
@property(nonatomic)UIPinchGestureRecognizer *pinchGesture;
@property(nonatomic,readonly)UICollectionViewTransitionLayout *transitionLayout;
@property(nonatomic)NSMutableArray *updateImagesBlocks;

@end

@implementation FSPVenuesViewController{
    UICollectionViewLayout *_originalLayout;
    UICollectionViewLayout *_transitLayout;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _venues = [NSMutableArray new];
    self.locationManager = ({
        CLLocationManager *manager = [CLLocationManager new];
        manager.delegate = self;
        [manager startUpdatingLocation];
        manager;
    });
    [self.collectionView registerNibWithClass:[FSPVenuePhotoCell class]];
    [self.collectionView registerNibWithClass:[FSPVenueHeaderCell class]
                   forSupplementaryViewOfKind:UICollectionElementKindSectionHeader];
    self.horizontalCellCount=4;
    self.collectionView.collectionViewLayout = [UICollectionViewFlowLayout layoutWithHorizontalItemCount:self.horizontalCellCount];
    [self enableGesture];

    self.searchBar.delegate=self;
    self.searchBar.enablesReturnKeyAutomatically=NO;
    
    self.refreshControl = ({
        UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
        [refreshControl addTarget:self
                           action:@selector(refreshAllVenues:)
                 forControlEvents:UIControlEventValueChanged];
        refreshControl;
    });
    self.updateImagesBlocks = [NSMutableArray new];
}


-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    self.location = locations[0];
}
-(void)viewWillAppear:(BOOL)animated
{
    if( self.venues.count == 0){
        [super viewWillAppear:animated];
        if( self.location){
            [self refreshAllVenues:nil];
        }else{
            [self addObserver:self
                   forKeyPath:@"location"
                      options:NSKeyValueObservingOptionNew
                      context:nil];
        }
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)observeValueForKeyPath:(NSString *)keyPath
                     ofObject:(id)object
                       change:(NSDictionary *)change
                      context:(void *)context
{
    if( [self observationInfo] ){
        [self removeObserver:self forKeyPath:@"location"];
    }
    [self refreshAllVenues:nil];
}
-(IBAction)refreshAllVenues:(id)sender
{
    [self.collectionView setContentOffset:CGPointMake(0.0f, -1*self.collectionView.contentInset.top) animated:YES];
    [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
    [Foursquare2 venueExploreByLocation:self.location
                             andKeyword:self.keyword
                               callback:^(BOOL success, id result){
                                   [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
                                   [self.refreshControl endRefreshing];
                                   if (success) {
                                       NSArray *newVenues = [FSPVenue venuesArrayFromJSON:result];
                                       [self updateVenuesAndSectionHeader:newVenues];
                                       for( NSUInteger section=0; section<self.venues.count; section++ ){
                                           FSPVenue *venue = self.venues[section];
                                           [Foursquare2 venueGetPhotos:venue.venueID
                                                                 limit:@(16)
                                                                offset:@(0)
                                                              callback:^(BOOL success, id result) {
                                                                  if( success ){
                                                                      NSArray *venueImages = [FSPVenuePhoto photoPathsFromJSON:result withVenue:venue];
                                                                      [self updateVenue:venue forImages:venueImages withSection:section];
                                                                  }else{
                                                                      // リトライ、どうする？
                                                                      NSLog( @"venueImage failed.");
                                                                  }
                                                              }];
                                       }
                                   }else{
                                       // リトライ、どうする？
                                   }
                               }];

}

-(void)updateVenuesAndSectionHeader:(NSArray *)newVenues
{
    [self.collectionView performBatchUpdates:^{
        if( self.venues.count != 0 ){
            NSIndexSet *indexSet =[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0,self.venues.count)];
            [self.collectionView deleteSections:indexSet];
        }
        [self.venues removeAllObjects];
        [self.venues addObjectsFromArray:newVenues];
        NSLog(@"%@",[self.venues description]);
        NSIndexSet *indexSet =[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0,self.venues.count)];
        [self.collectionView insertSections:indexSet];
    } completion:nil];
}

-(void)updateVenue:(FSPVenue *)venue forImages:(NSArray *)newImages withSection:(NSUInteger)section
{
    void(^blk)() =^{
        // 古いvenueのphotosを除去
        if( venue.photos.count != 0 ){
            NSArray *deleteRows = [NSIndexPath arrayWithSection:section andRowCount:venue.photos.count];
            [self.collectionView deleteItemsAtIndexPaths:deleteRows];
        }
        
        // 新しいphotosを追加
        venue.photos = newImages;
        
        if( venue.photos.count != 0 ){
            NSArray *insertRows = [NSIndexPath arrayWithSection:section andRowCount:venue.photos.count];
            [self.collectionView insertItemsAtIndexPaths:insertRows];
        }
    };
    if( self.transitionLayout != nil ){
        // アニメーション中の場合は、アニメーションが終わってから実行する
        [self.updateImagesBlocks addObject:blk];
        return;
    }else{
        [self.collectionView performBatchUpdates:blk
                                      completion:nil];
    }
//     ^{
//        // 古いvenueのphotosを除去
//        if( venue.photos.count != 0 ){
//            NSArray *deleteRows = [NSIndexPath arrayWithSection:section andRowCount:venue.photos.count];
//            [self.collectionView deleteItemsAtIndexPaths:deleteRows];
//        }
//        
//        // 新しいphotosを追加
//        venue.photos = newImages;
//        
//        if( venue.photos.count != 0 ){
//            NSArray *insertRows = [NSIndexPath arrayWithSection:section andRowCount:venue.photos.count];
//            [self.collectionView insertItemsAtIndexPaths:insertRows];
//        }
//    }
//                                  completion:nil];
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue
                sender:(id)sender
{
    NSLog( @"%@",sender);
    UIViewController *c = (UIViewController *)[segue destinationViewController];
    if( [c isKindOfClass: FSPPhotosViewController.class] ){
        FSPPhotosViewController *largePhotoViewController = (FSPPhotosViewController *)c;
        FSPVenuePhotoCell *cell = (FSPVenuePhotoCell *)sender;
        FSPVenue *venue = cell.venueImage.venue;
        if( venue ){
            largePhotoViewController.venueImages = venue.photos;
            largePhotoViewController.venueImagesIndex = cell.tag;
        }
    }else if( [c isKindOfClass:FSPMapViewController.class]){
        FSPMapViewController *mapViewController = (FSPMapViewController *)c;
        NSIndexPath *path = (NSIndexPath *)sender;
        mapViewController.venue =  self.venues[path.section];
        
    }else if( [c isKindOfClass:PBWebViewController.class]){
        NSIndexPath *path = (NSIndexPath *)sender;
        FSPVenue *venue = self.venues[path.section];
        PBWebViewController *webViewController = (PBWebViewController *)c;
        webViewController.URL = venue.url;
        webViewController.showsNavigationToolbar=YES;
    }
}

#pragma mark - Gesture controll.
-(void)enableGesture
{
    if( self.pinchGesture == nil ){
        self.pinchGesture = [UIPinchGestureRecognizer new];
        [self.pinchGesture addTarget:self action:@selector(pinchAction:)];
    }
    [self.collectionView addGestureRecognizer:self.pinchGesture];
}
-(void)disableGesture
{
    [self.collectionView removeGestureRecognizer:self.pinchGesture];
}
#define kPAMProgressThreshold 0.5
-(void)pinchAction:(UIPinchGestureRecognizer *)gesture
{
    switch(gesture.state){
        case UIGestureRecognizerStateBegan:
        {
            NSLog(@"begin scale=%f",gesture.scale);
            // 拡大なのか縮小なのかを記録しておく。
            // これをしておかないと、拡大で開始して途中で縮小したりするとおかしな事になる。
            self.zoomingStatus = gesture.pam_zoomStatus;
            
            // Transitionする先のLayoutを用意
            NSUInteger nextItemCount = [self nextHoraizontalItemCount];
            UICollectionViewLayout *nextLayout=[UICollectionViewFlowLayout layoutWithHorizontalItemCount:nextItemCount];
            
            // Transitionの開始
            [self.collectionView startInteractiveTransitionToCollectionViewLayout:nextLayout
                                                                       completion:^(BOOL completed, BOOL finish) {
                                                                           NSLog( @"completion");
                                                                           // Transitionの完了時にジェスチャーを止めるので、ここで再開
                                                                           [self enableGesture];
                                                                           // 止めていたImageの更新を再開
                                                                           for (void(^blk)() in self.updateImagesBlocks) {
                                                                               [self.collectionView performBatchUpdates:blk
                                                                                                             completion:nil];
                                                                           }
                                                                           [self.updateImagesBlocks removeAllObjects];
                                                                       }];
        }
            break;
        case UIGestureRecognizerStateChanged:
            // Transitionの進捗(progress)を0.0〜1.0で更新
            self.transitionLayout.transitionProgress = [gesture pam_transitionProgressWithZoomStatus:self.zoomingStatus];
            NSLog( @"transitionProgress=%f",self.transitionLayout.transitionProgress);
            break;
        case UIGestureRecognizerStateEnded:
            // Transitionを完了
            if( self.transitionLayout.transitionProgress > kPAMProgressThreshold ){
                [self.collectionView finishInteractiveTransition];
                self.horizontalCellCount = [self nextHoraizontalItemCount];
            }else{
                [self.collectionView cancelInteractiveTransition];
            }
            // 最後のアニメーションが終わるまでジェスチャーを停止
            [self disableGesture];
            break;
        default:
            break;
    }
}
-(NSUInteger)nextHoraizontalItemCount
{
    return [self nextHorizontalCellCountWithZoomStatus:self.zoomingStatus];
//    NSUInteger nextCount;
//    switch (self.zoomingStatus) {
//        case PAMPinchGestureZoomStatusZoomIn:
//            nextCount = self.horizontalCellCount-1;
//            nextCount = nextCount==0 ? 1 : nextCount;
//            break;
//        case PAMPinchGestureZoomStatusZoomOut:
//            nextCount = self.horizontalCellCount+1;
//            break;
//    }
//    return nextCount;
}

-(UICollectionViewTransitionLayout *)transitionLayout
{
    id layout = self.collectionView.collectionViewLayout;
    if( [layout isKindOfClass:[UICollectionViewTransitionLayout class]]){
        return layout;
    }else{
        return nil;
    }
}


#pragma mark - UICollectionViewDataSource

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    FSPVenue *venue = self.venues[indexPath.section];
    if( indexPath.row == venue.photos.count+1 ){
        // 詳細タップ
        [self performSegueWithIdentifier:@"PBWebViewController" sender:indexPath];
    }else if( indexPath.row == venue.photos.count ){
        [self performSegueWithIdentifier:@"FSPMapViewController" sender:indexPath];
    }else{
        if( self.transitionLayout != nil ){
            // アニメーション中の場合は何もしない
            return;
        }
        self.horizontalCellCount = [self nextHorizontalCellCount];
        UICollectionViewLayout *nextLayout = [UICollectionViewFlowLayout layoutWithHorizontalItemCount:self.horizontalCellCount];
        [self.collectionView setCollectionViewLayout:nextLayout
                                            animated:YES
                                          completion:nil];
         
    }
}

-(NSUInteger)nextHorizontalCellCount
{
    return [self nextHorizontalCellCountWithZoomStatus:PAMPinchGestureZoomStatusZoomIn];
//    switch (self.horizontalCellCount) {
//        case 4:
//        case 3:
//            return 2;
//            break;
//        case 2:
//            return 1;
//            break;
//        case 1:
//            return 1;
//            break;
//        default:
//            return 4;
//            break;
//    }
}

-(NSInteger)nextHorizontalCellCountWithZoomStatus:(PAMPinchGestureZoomStatus)zoomStatus
{
    NSUInteger count=1;
    if( zoomStatus == PAMPinchGestureZoomStatusZoomIn ){
        count = self.horizontalCellCount/2;
    }else{
        count = self.horizontalCellCount*2;
    }
    count = count<1 ? 1 : count;
    count = count>16 ? 16 : count;
    return count;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return self.venues.count;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView 
    numberOfItemsInSection:(NSInteger)section
{
    FSPVenue *venue = self.venues[section];
    return venue.photos.count;
}
-(UICollectionReusableView *)collectionView:(UICollectionView *)collectionView
          viewForSupplementaryElementOfKind:(NSString *)kind
                                atIndexPath:(NSIndexPath *)indexPath
{
    FSPVenueHeaderCell *header = [collectionView dequeueReusableSupplementaryViewOfKind:kind
                                                                              withClass:[FSPVenueHeaderCell class]
                                                                           forIndexPath:indexPath];
    header.venue = self.venues[indexPath.section];
    header.nameButton.tag = indexPath.section;
    [header.nameButton addTarget:self
                          action:@selector(touchNameButton:)
                forControlEvents:UIControlEventTouchUpInside];
    header.arrowButton.tag = indexPath.section;
    [header.arrowButton addTarget:self
                           action:@selector(touchArrowButton:)
                 forControlEvents:UIControlEventTouchUpInside];
    return header;
}

-(void)touchNameButton:(id)sender
{
    NSIndexPath *indexPath = ({
        UIButton *button = (UIButton *)sender;
        [NSIndexPath indexPathForRow:0 inSection:button.tag];
    });
    [self performSegueWithIdentifier:@"PBWebViewController" sender:indexPath];
}

-(void)touchArrowButton:(id)sender
{
    NSIndexPath *indexPath = ({
        UIButton *button = (UIButton *)sender;
        [NSIndexPath indexPathForRow:0 inSection:button.tag];
    });
    [self performSegueWithIdentifier:@"FSPMapViewController" sender:indexPath];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                 cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    FSPVenue *venue = self.venues[indexPath.section];
    return  ({
        FSPVenuePhotoCell *cell = [collectionView dequeueReusableCellWithClass:[FSPVenuePhotoCell class]
                                                                  forIndexPath:indexPath];
        FSPVenuePhoto *image = venue.photos[indexPath.row];
        cell.venueImage = image;
        cell.tag = indexPath.row;
        cell;
    });
}
#pragma mark - UIScrollViewDelegate
// 一時的に外す：隠すロジックはそのうち再実装。
//-(void)scrollViewDidScroll:(UIScrollView *)scrollView
//{
//
//    if (scrollView.contentOffset.y > 10 ){
//        // 10px以上下に行った
//        if( !self.navigationController.navigationBarHidden ){
//            // 隠れてなければかくれろ。
//            [self.navigationController setNavigationBarHidden:YES animated:YES];
//        }
//    }else{
//        // 10px未満
//        if ( self.navigationController.navigationBarHidden ){
//            // 隠れてれば出てこい。
//            [self.navigationController setNavigationBarHidden:NO animated:YES];
//        }
//    }
//}
#pragma mark - UISearchBarDelegate
-(BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    [searchBar setShowsCancelButton:YES];
    return YES;
}
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    NSLog( @"key=%@",searchBar.text);
    [searchBar resignFirstResponder];
    [searchBar setShowsCancelButton:NO];
    if( searchBar.text != nil ){
        self.keyword = searchBar.text;
        [self refreshAllVenues:nil];
    }
}
-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    [searchBar setShowsCancelButton:NO];
}
@end
