//
//  FSPLargePhotoViewController.m
//  4sqPhoto
//
//  Created by tak on 2014/01/20.
//  Copyright (c) 2014年 taktamur. All rights reserved.
//

#import "FSPPhotosViewController.h"
#import "FSPVenuePhotoCell.h"
#import "UIKit+Util.h"

@interface FSPPhotosViewController ()

@end

@implementation FSPPhotosViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.collectionView registerNibWithClass:[FSPVenuePhotoCell class]];

}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    NSIndexPath *path = [NSIndexPath indexPathForRow:self.venueImagesIndex inSection:0];
    [self.collectionView scrollToItemAtIndexPath:path
                                atScrollPosition:UICollectionViewScrollPositionRight
                                        animated:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UICollectionViewDataSource
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
-(NSInteger)collectionView:(UICollectionView *)collectionView
    numberOfItemsInSection:(NSInteger)section
{
    return self.venueImages.count;
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                 cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    FSPVenuePhotoCell *cell =
        [collectionView dequeueReusableCellWithClass:[FSPVenuePhotoCell class]
                                        forIndexPath:indexPath] ;

    cell.venueImage = self.venueImages[indexPath.row];
    return cell;
}
@end
