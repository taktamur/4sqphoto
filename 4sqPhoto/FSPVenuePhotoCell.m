//
//  FSPPhotoViewCell.m
//  4sqPhoto
//
//  Created by tak on 2014/01/18.
//  Copyright (c) 2014年 taktamur. All rights reserved.
//

#import "FSPVenuePhotoCell.h"
#import <AFNetworking/UIImageView+AFNetworking.h>

@interface FSPVenuePhotoCell()
@property (weak, nonatomic) IBOutlet UIImageView *photoView;

@end
@implementation FSPVenuePhotoCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self){
        self.photoView.clipsToBounds=YES;
        self.photoView.contentMode=UIViewContentModeScaleAspectFill;
    }
    return self;
}

-(void)setVenueImage:(FSPVenuePhoto *)venueImage
{
    if( _venueImage != venueImage ){
        _venueImage = venueImage;
        self.photoView.alpha=0;
        CGFloat scale = [UIScreen mainScreen].scale;
        NSURL *imageURL =[venueImage urlWithSize:self.bounds.size scale:scale];
        [self.photoView setImageWithURLRequest:[NSURLRequest requestWithURL:imageURL]
                              placeholderImage:nil
                                       success:^(NSURLRequest *request,
                                                 NSHTTPURLResponse *response,
                                                 UIImage *image) {
                                           [UIView animateWithDuration:0.3
                                                            animations:^{
                                                                self.photoView.alpha=1;
                                                                [self.photoView setImage:image];
                                                            }];
                                       } failure:nil];
    }
}
@end
