//
//  FSPVenueMapCell.h
//  4sqPhoto
//
//  Created by Takafumi Tamura on 2014/01/22.
//  Copyright (c) 2014年 taktamur. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface FSPVenueMapCell : UICollectionViewCell
-(void)setVenueLocation:(CLLocation *)location;
@end
