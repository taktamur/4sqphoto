//
//  FSPsectionView.m
//  4sqPhoto
//
//  Created by tak on 2014/01/18.
//  Copyright (c) 2014年 taktamur. All rights reserved.
//

#import "FSPVenueHeaderCell.h"
#import <AFNetworking/UIImageView+AFNetworking.h>
#import "CLLocation+Direction.h"
#import "FSPArrowView.h"

@interface FSPVenueHeaderCell()
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;
@property (weak, nonatomic) IBOutlet UILabel *tipsLabel;
@property (nonatomic)CLLocationManager *locationManager;
@end

@implementation FSPVenueHeaderCell{
    FSPVenue *_venue;
}


-(void)setVenue:(FSPVenue *)venue
{
    if( self.locationManager == nil ){
        self.locationManager = [CLLocationManager new];
        self.locationManager.delegate = self;
        [self.locationManager startUpdatingLocation];
        [self.locationManager startUpdatingHeading];
    }

    if( _venue != venue ){
        _venue = venue;
//        self.nameLabel.text = _venue.name;
        [self.nameButton setTitle:venue.name forState:UIControlStateNormal];
        self.nameButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        UIBezierPath *arrowPath = [FSPArrowView arrowBezierWithSize:self.arrowButton.bounds.size
                                                           andColor:self.tintColor];
        UIImage *arrowImage = [FSPArrowView imageBySize:self.arrowButton.bounds.size
                                          andBezierPath:arrowPath];
        [self.arrowButton setImage:arrowImage forState:UIControlStateNormal];
        self.distanceLabel.text = @"";
//        [self.categoryIconImageView setImageWithURL:
//         [self.venue categoryIconURLWithSize:self.categoryIconImageView.bounds.size.width]];
        if( venue.tips.count > 0 ){
            self.tipsLabel.text = [NSString stringWithFormat:@"\"%@",
                                   ((FSPVenueTips *)venue.tips[0]).text ];
        }else{
            self.tipsLabel.text=nil;
        }
    }
    
}

//-(void)drawRect:(CGRect)rect
//{
//    UIBezierPath *bezierPath = [UIBezierPath bezierPath];
//    [bezierPath moveToPoint:    CGPointMake(0,1)];
//    [bezierPath addLineToPoint: CGPointMake(self.bounds.size.width,1)];
//    [bezierPath closePath];
//    UIColor *color = [UIColor orangeColor];
//    [color setStroke];
//    bezierPath.lineWidth=1;
//    [bezierPath stroke];
//
//    [super drawRect:rect];
//
//}
#pragma mark - CLLocationManagerDelegate
-(void)locationManager:(CLLocationManager *)manager
    didUpdateLocations:(NSArray *)locations
{
    CLLocation *location = locations[0];
    CLLocationDistance distanceMeter = [location distanceFromLocation:self.venue.location];
    self.distanceLabel.text = [NSString stringWithFormat:@"%dm",(int)distanceMeter];
    
}
-(void)locationManager:(CLLocationManager *)manager
      didUpdateHeading:(CLHeading *)newHeading
{
//    NSLog( @"update heading");
    CLLocationDirection heading = newHeading.trueHeading; // 0.0〜360.0
    CLLocationDirection target = [manager.location aboutAzimuthToLocation:self.venue.location];
    CLLocationDirection displayDirection = target-heading;
//    NSLog( @"venur=%@ heading=%f, target=%f, display=%f",self.venue.name,heading,target,displayDirection);
    self.arrowButton.transform = CGAffineTransformMakeRotation(M_PI * displayDirection / 180.0);
    
    
}

@end
