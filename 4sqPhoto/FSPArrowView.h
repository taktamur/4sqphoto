//
//  FSPArrowView.h
//  4sqPhoto
//
//  Created by tak on 2014/01/26.
//  Copyright (c) 2014年 taktamur. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FSPArrowView : UIView
+(UIBezierPath *)arrowBezierWithSize:(CGSize)size andColor:(UIColor *)color;
+(UIImage *)imageBySize:(CGSize)size andBezierPath:(UIBezierPath *)bezierPath;

@end
