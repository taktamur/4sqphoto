//
//  FSPVenue.m
//  4sqPhoto
//
//  Created by tak on 2014/01/18.
//  Copyright (c) 2014年 taktamur. All rights reserved.
//

#import "FSPVenue.h"

/** 
 * 店舗のModel
 */
@interface FSPVenue()
@property(nonatomic,readwrite) NSURL *photoURL;
@end

@implementation FSPVenue

- (id)init
{
    self = [super init];
    if (self) {
        _photos = [NSMutableArray new];
    }
    return self;
}

-(NSURL *)categoryIconURLWithSize:(NSUInteger)size
{
    NSString *urlString = [NSString stringWithFormat:@"%@%lux%lu%@",
                           self.categoryIconPrefix,
                           (unsigned long)size,
                           (unsigned long)size,
                           self.categoryIconSuffix];
    return [NSURL URLWithString:urlString];
}

-(NSURL *)url
{
    NSString *urlString = [NSString stringWithFormat:@"http://foursquare.com/v/%@",
                           self.venueID];
    return [NSURL URLWithString:urlString];
}

-(NSString *)description
{
    return [NSString stringWithFormat:@"%@",_name];
}

@end

/**
 * 写真のModel
 */
@implementation FSPVenuePhoto{
    NSString *_prefix;
    NSString *_suffix;
}
@synthesize venue;

-(id)initWithPrefix:(NSString *)prefix andSuffix:(NSString *)suffix
{
    self = [super init];
    if (self) {
        _prefix=prefix;
        _suffix=suffix;
    }
    return self;
    
}
-(NSURL *)urlWithSize:(CGSize)size
{
    return [self urlWithSize:size scale:1.0];
}

-(NSURL *)urlWithSize:(CGSize)size scale:(CGFloat)scale
{
    
    NSString *urlString = [NSString stringWithFormat:@"%@%lux%lu%@",
                           _prefix,
                           (unsigned long)(size.width*scale),
                           (unsigned long)(size.height*scale),
                           _suffix];
    return [NSURL URLWithString:urlString];
}
@end

/**
 * Tipsのmodel
 */
@implementation FSPVenueTips
@synthesize text;
@end

/**
 * 地図表示のModel
 */
@implementation FSPVenueMapImage
@synthesize venue;

static NSString *_appid =@"i77tRH2xg65djdzHpP2M8c2r2T.lVgKVW_r8Lvz_Yk_H6qpDKr__nMkWYB..nA--";
-(NSURL *)urlWithSize:(CGSize)size
{
    NSString *url = [NSString stringWithFormat:@"http://map.olp.yahooapis.jp/OpenLocalPlatform/V1/static?appid=%@&lat=%f&lon=%f&z=16&width=%lu&height=%lu&pointer=on",
                     _appid,
                     self.venue.location.coordinate.latitude,
                     self.venue.location.coordinate.longitude,
                     (unsigned long)size.width,
                     (unsigned long)size.height];
    return [NSURL URLWithString:url];
}
@end

// ------------------------
#pragma mark - parse json.
// ------------------------
@implementation FSPVenue(JSONParse)
+(NSArray *)venuesArrayFromJSON:(id)json
{
    NSDictionary *dic = json;
    NSArray *venuesJson = [dic valueForKeyPath:@"response.groups"][0][@"items"];

    return _.array(venuesJson).map(^FSPVenue *(NSDictionary *dic){
        FSPVenue *venue = [FSPVenue venueFromJSONDictionary:dic];
        return venue;
    }).unwrap;

}

// https://developer.foursquare.com/docs/venues/explore
+(FSPVenue *)venueFromJSONDictionary:(NSDictionary *)dic
{
    NSDictionary *venueDictionary = dic[@"venue"];
    NSArray  *tipsArray = dic[@"tips"];
    return ({
        FSPVenue *ann = [[FSPVenue alloc]init];
        ann.name    = venueDictionary[@"name"];
        ann.venueID = venueDictionary[@"id"];
        ann.location = ({
            double lat = [(NSNumber *)venueDictionary[@"location"][@"lat"] doubleValue];
            double lon = [(NSNumber *)venueDictionary[@"location"][@"lng"] doubleValue];
            [[CLLocation alloc]initWithLatitude:lat longitude:lon];
        });
        ann.categoryIconPrefix = venueDictionary[@"categories"][0][@"icon"][@"prefix"];
        ann.categoryIconSuffix = venueDictionary[@"categories"][0][@"icon"][@"suffix"];
        ann.tips = ({
            _.array(tipsArray).map(^FSPVenueTips *(NSDictionary *dic){
                FSPVenueTips *tips = [FSPVenueTips new];
                tips.text = dic[@"text"];
                return tips;
            }).unwrap;
        });
        ann;
    });

}
@end

@implementation FSPVenuePhoto(JSONParse)
+(NSArray *)photoPathsFromJSON:(id)json withVenue:(FSPVenue *)venue
{
    NSArray *photosJSONs = [json valueForKeyPath:@"response.photos.items"];
    return _.array(photosJSONs).map(^FSPVenuePhoto *(NSDictionary *json){
        FSPVenuePhoto *image = [[FSPVenuePhoto alloc]initWithPrefix:json[@"prefix"]
                                                          andSuffix:json[@"suffix"]];
        image.venue = venue;
        return image;
    }).unwrap;
}
@end


/**
 tips: [
 {
    id: "4e3c1096091a0fc67ea2e08a"
    createdAt: 1312559254
    text: "Michael Psilakis cools off at the Brooklyn Ice Cream Factory in where they serve up a scoop of Butter Pecan Ice Cream on Food Network's The Best Thing I Ever Ate. Find more tips at FN Local."
    url: "http://www.foodnetwork.com/local/index.html"
    canonicalUrl: "https://foursquare.com/item/4e3c1096091a0fc67ea2e08a"
    likes: {
        count: 103
        groups: [ ]
        summary: "103 人が「いいね！」と言っています。"
    }
    logView: true
    todo: {
        count: 25
    }
    user: {
        id: "4362480"
        firstName: "Food Network"
        gender: "none"
        photo: {
            prefix: "https://irs1.4sqi.net/img/user/"
            suffix: "/CWXVNR3ZCNNEZ45T.jpg"
        }
        type: "page"
    }
 }
 ]
 */


