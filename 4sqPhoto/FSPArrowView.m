//
//  FSPArrowView.m
//  4sqPhoto
//
//  Created by tak on 2014/01/26.
//  Copyright (c) 2014年 taktamur. All rights reserved.
//

#import "FSPArrowView.h"
#import "UIKit+Util.h"

@implementation FSPArrowView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
    UIBezierPath *bezierPath = [self.class arrowBezierWithSize:self.bounds.size
                                                      andColor:[UIColor orangeColor]];
    [bezierPath stroke];
    [super drawRect:rect];
}

+(UIImage *)imageBySize:(CGSize)size andBezierPath:(UIBezierPath *)bezierPath
{
    
    // begin graphics context for drawing
    UIGraphicsBeginImageContextWithOptions(size, NO, 0);
    
    // get reference to the graphics context
    CGContextRef context = UIGraphicsGetCurrentContext();
    

    CGContextSaveGState(context);
    
//    [color setStroke];
    [bezierPath stroke];
    [bezierPath fill];

    // get an image of the graphics context
    UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
    
    // end the context
    UIGraphicsEndImageContext();
    
    return viewImage;
}

+(UIBezierPath *)arrowBezierWithSize:(CGSize)size andColor:(UIColor *)color{

    CGFloat path[][2] = {
        {0.5,0.1},
        {0.1,0.5},
        {0.3,0.5},
        {0.3,0.9},
        {0.7,0.9},
        {0.7,0.5},
        {0.9,0.5},
        {0.5,0.1}
    };
    
    UIBezierPath *bezierPath = [UIBezierPath bezierPath];
    [bezierPath moveToPoint: CGPointMake(size.width * path[0][0], size.height * path[0][1])];
    for( int i=1; i<sizeof(path)/sizeof(path[0]); i++ ){
        [bezierPath addLineToPoint:CGPointMake(size.width*path[i][0], size.height*path[i][1])];
    }
    [bezierPath closePath];
    bezierPath.lineWidth=1;
    return bezierPath;
}

@end
