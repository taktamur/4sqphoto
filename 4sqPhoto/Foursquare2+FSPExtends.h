//
//  Foursquare2+FSPExtends.h
//  4sqPhoto
//
//  Created by tak on 2014/01/18.
//  Copyright (c) 2014年 taktamur. All rights reserved.
//

#import "Foursquare2.h"
#import <CoreLocation/CoreLocation.h>

@interface Foursquare2 (FSPExtends)
+(void)venueExploreByLocation:(CLLocation *)location
                                    callback:(Foursquare2Callback)callback;
+(void)venueExploreByLocation:(CLLocation *)location
                   andKeyword:(NSString *)keyword
                     callback:(Foursquare2Callback)callback;

@end
