
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// AFNetworking
#define COCOAPODS_POD_AVAILABLE_AFNetworking
#define COCOAPODS_VERSION_MAJOR_AFNetworking 2
#define COCOAPODS_VERSION_MINOR_AFNetworking 0
#define COCOAPODS_VERSION_PATCH_AFNetworking 3

// AFNetworking/NSURLConnection
#define COCOAPODS_POD_AVAILABLE_AFNetworking_NSURLConnection
#define COCOAPODS_VERSION_MAJOR_AFNetworking_NSURLConnection 2
#define COCOAPODS_VERSION_MINOR_AFNetworking_NSURLConnection 0
#define COCOAPODS_VERSION_PATCH_AFNetworking_NSURLConnection 3

// AFNetworking/NSURLSession
#define COCOAPODS_POD_AVAILABLE_AFNetworking_NSURLSession
#define COCOAPODS_VERSION_MAJOR_AFNetworking_NSURLSession 2
#define COCOAPODS_VERSION_MINOR_AFNetworking_NSURLSession 0
#define COCOAPODS_VERSION_PATCH_AFNetworking_NSURLSession 3

// AFNetworking/Reachability
#define COCOAPODS_POD_AVAILABLE_AFNetworking_Reachability
#define COCOAPODS_VERSION_MAJOR_AFNetworking_Reachability 2
#define COCOAPODS_VERSION_MINOR_AFNetworking_Reachability 0
#define COCOAPODS_VERSION_PATCH_AFNetworking_Reachability 3

// AFNetworking/Security
#define COCOAPODS_POD_AVAILABLE_AFNetworking_Security
#define COCOAPODS_VERSION_MAJOR_AFNetworking_Security 2
#define COCOAPODS_VERSION_MINOR_AFNetworking_Security 0
#define COCOAPODS_VERSION_PATCH_AFNetworking_Security 3

// AFNetworking/Serialization
#define COCOAPODS_POD_AVAILABLE_AFNetworking_Serialization
#define COCOAPODS_VERSION_MAJOR_AFNetworking_Serialization 2
#define COCOAPODS_VERSION_MINOR_AFNetworking_Serialization 0
#define COCOAPODS_VERSION_PATCH_AFNetworking_Serialization 3

// AFNetworking/UIKit
#define COCOAPODS_POD_AVAILABLE_AFNetworking_UIKit
#define COCOAPODS_VERSION_MAJOR_AFNetworking_UIKit 2
#define COCOAPODS_VERSION_MINOR_AFNetworking_UIKit 0
#define COCOAPODS_VERSION_PATCH_AFNetworking_UIKit 3

// CrittercismSDK
#define COCOAPODS_POD_AVAILABLE_CrittercismSDK
#define COCOAPODS_VERSION_MAJOR_CrittercismSDK 4
#define COCOAPODS_VERSION_MINOR_CrittercismSDK 3
#define COCOAPODS_VERSION_PATCH_CrittercismSDK 1

// FSOAuth
#define COCOAPODS_POD_AVAILABLE_FSOAuth
#define COCOAPODS_VERSION_MAJOR_FSOAuth 1
#define COCOAPODS_VERSION_MINOR_FSOAuth 0
#define COCOAPODS_VERSION_PATCH_FSOAuth 0

// Foursquare-API-v2
#define COCOAPODS_POD_AVAILABLE_Foursquare_API_v2
#define COCOAPODS_VERSION_MAJOR_Foursquare_API_v2 1
#define COCOAPODS_VERSION_MINOR_Foursquare_API_v2 4
#define COCOAPODS_VERSION_PATCH_Foursquare_API_v2 1

// PBWebViewController
#define COCOAPODS_POD_AVAILABLE_PBWebViewController
#define COCOAPODS_VERSION_MAJOR_PBWebViewController 0
#define COCOAPODS_VERSION_MINOR_PBWebViewController 1
#define COCOAPODS_VERSION_PATCH_PBWebViewController 0

// TestFlightSDK
#define COCOAPODS_POD_AVAILABLE_TestFlightSDK
#define COCOAPODS_VERSION_MAJOR_TestFlightSDK 2
#define COCOAPODS_VERSION_MINOR_TestFlightSDK 2
#define COCOAPODS_VERSION_PATCH_TestFlightSDK 1

// Underscore.m
#define COCOAPODS_POD_AVAILABLE_Underscore_m
#define COCOAPODS_VERSION_MAJOR_Underscore_m 0
#define COCOAPODS_VERSION_MINOR_Underscore_m 2
#define COCOAPODS_VERSION_PATCH_Underscore_m 1

