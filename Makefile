# http://hakobe932.hatenablog.com/entry/2012/01/21/142726#fn1

.PHONY: all build archive testflight 
PROJECT ?= 4sqPhoto.xcworkspace
SCHEME ?= 4sqPhoto
SIGN ?= iOS Developer
DSYM = $(PWD)/out/build/4sqPhoto.app.dSYM

all: archive

out:
	mkdir -p $@

build: out
	xcodebuild -workspace '$(PROJECT)' -scheme '$(SCHEME)' -sdk iphoneos7.0 install DSTROOT=out CONFIGURATION_BUILD_DIR=$(PWD)/out/build

archive: build
	xcrun -sdk iphoneos7.0 PackageApplication 'out/Applications/$(SCHEME).app' -o '$(PWD)/out/$(SCHEME).ipa'

zip-dsym: build
	zip -r $(PWD)/out/$(SCHEME).dSYM.zip $(DSYM)

testflight: archive zip-dsym
	curl 'http://testflightapp.com/api/builds.json' \
	-F 'file=@$(PWD)/out/$(SCHEME).ipa' \
	-F 'dsym=@$(PWD)/out/$(SCHEME).dSYM.zip' \
	-F 'api_token=a296a03a-71ec-46b5-b85a-532f62821dcf' \
	-F 'team_token=c2e77f8bf71c1d4b280220be1d87a80b_MjU4ODg1MjAxMy0wOC0xMyAwNjozODoyNC41MTkxOTA' \
	-F 'notes=This build was uploaded via the upload API' \
	-v

clean:
	rm -rf ./out